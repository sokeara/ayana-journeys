<?php
/*
Template Name: User Guide Line
*/
?>
<!DOCTYPE html>
<html>
<head>
	<title>User Guide</title>
	<style type="text/css">
		body {
			background-color: #F2F3F7;
		}

		img {
			max-width: 100%;
			height: auto;
		}

		a {
			color: #448CCB;
			text-decoration: none;
		}

		a:hover {
			color: #609;
			text-decoration: underline;
		}

		.user-guide {
			border: 1px solid #448CCB;
			margin: 0 auto;
			height: auto;
			padding: 0px 20px 20px 20px;
		 	width: 80%;
		}

		.link-title {
			padding: 5px;
		}

		.box-container {
			padding: 5px;
			width: 100%;
		}
	</style>
</head>
<body>
	<div class="user-guide">
		<h1 align="center">User Guide Documentation</h1>

		<!-- Step 01 -->
		<div class="link-title">
			<a href="javascript:showcontent('step1');"><span>&#187;</span> Documentation 01</a>
		</div>
		<div class="content-box" id="step1" style="display: none;">
			<div class="box-container">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic1.jpg" alt="Image">
				<p>- Descript about your document picture</p>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic1.png" alt="Image">
				<p>1. .....</p>
				<p>2. .....</p>
				<p>3. .....</p>
			</div>
		</div>

		<!-- Step 02 -->
		<div class="link-title">
			<a href="javascript:showcontent('step2');"><span>&#187;</span> Documentation 02</a>
		</div>
		<div class="content-box" id="step2" style="display: none;">
			<div class="box-container">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic1.jpg" alt="Image">
				<p>- Descript about your document picture</p>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic1.png" alt="Image">
				<p>1. .....</p>
				<p>2. .....</p>
				<p>3. .....</p>
			</div>
		</div>

		<!-- Step 03 -->
		<div class="link-title">
			<a href="javascript:showcontent('step3');"><span>&#187;</span> Documentation 03</a>
		</div>
		<div class="content-box" id="step3" style="display: none;">
			<div class="box-container">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic1.jpg" alt="Image">
				<p>- Descript about your document picture</p>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic1.png" alt="Image">
				<p>1. .....</p>
				<p>2. .....</p>
				<p>3. .....</p>
			</div>
		</div>

		<!-- Step 04 -->
		<div class="link-title">
			<a href="javascript:showcontent('step4');"><span>&#187;</span> Documentation 04</a>
		</div>
		<div class="content-box" id="step4" style="display: none;">
			<div class="box-container">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic1.jpg" alt="Image">
				<p>- Descript about your document picture</p>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic1.png" alt="Image">
				<p>1. .....</p>
				<p>2. .....</p>
				<p>3. .....</p>
			</div>
		</div>

		<!-- Step 05 -->
		<div class="link-title">
			<a href="javascript:showcontent('step5');"><span>&#187;</span> Documentation 05</a>
		</div>
		<div class="content-box" id="step5" style="display: none;">
			<div class="box-container">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic1.jpg" alt="Image">
				<p>- Descript about your document picture</p>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic1.png" alt="Image">
				<p>1. .....</p>
				<p>2. .....</p>
				<p>3. .....</p>
			</div>
		</div>
	</div>
</body>
<script language="javascript"> 
	function showcontent(thechosenone) {
		var newboxes = document.getElementsByTagName("div");
		for(var x=0; x<newboxes.length; x++) {
			name = newboxes[x].getAttribute("class");
			if (name == 'content-box') {
				if (newboxes[x].id == thechosenone) {
					if (newboxes[x].style.display == 'block') {
						newboxes[x].style.display = 'none';
					}
					else {
						newboxes[x].style.display = 'block';
					}
				}else {
					newboxes[x].style.display = 'none';
				}
			}
		}
	}
</script>
</html>