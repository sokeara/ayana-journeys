<?php
/*
Template Name: Sitemap Template
*/
get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
				<div class="entry-content">
					<div id="sitemap">
						<h2 id="pages">Sitemap</h2>
						<p>Brow our sitemap below:</p>
						<ul>
							<?php
							// Add pages you'd like to exclude in the exclude here
								wp_list_pages(
								  	array(
								    	'exclude' => '',
								    	'title_li' => '',
								  	)
								);
							?>
						</ul>
	                </div><!-- .sitemap -->
				</div> <!-- .entry-content -->
			</div> <!-- #left-area -->
			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>